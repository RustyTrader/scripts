﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // This file contains your actual script.
        //
        // You can either keep all your code here, or you can create separate
        // code files to make your program easier to navigate while coding.
        //
        // In order to add a new utility class, right-click on your project, 
        // select 'New' then 'Add Item...'. Now find the 'Space Engineers'
        // category under 'Visual C# Items' on the left hand side, and select
        // 'Utility Class' in the main area. Name it in the box below, and
        // press OK. This utility class will be merged in with your code when
        // deploying your final script.
        //
        // You can also simply create a new utility class manually, you don't
        // have to use the template if you don't want to. Just do so the first
        // time to see what a utility class looks like.
        // 
        // Go to:
        // https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
        //
        // to learn more about ingame scripts.

        // Rusty Script to Move Cargo to Waste Bin .ie. Stone & Ice When the Threshold of Cargo is reached
        // Setup
        string SorterPrefix = "SorterWaste";    // Sorter to be Turned on for the Waste Move
        string ContPrefix = "LCargoWaste";      // Containers to Hold the Items being Sent for Wasteing. (To)
        string LookInPrefix = "LCargoOre";      // Prefix for Contoianers to look at (From)
        string LCD_Name = "LCD_Waste_Track";    // LCD for Outputing the Waste Information
        int InvMulty = 10;                      // Server Cargo Inventory Multiplyer
                                                // System will Calculate for Server using InvMulty * Dump Amount
        int StoneDumpOn = 3135000;     //  +- 2.75 Containers of Stone & 1x Inventory 
        int IceDumpOn = 3135000;       //  +- 2.75 Containers of Ice
        int myCnt = 100;
        const int cMaxLoops = 5;   // No of Loops Between Updates

        // Script Starts Here 

        //---------------------       




        MyWaste thisWaste;

        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set Runtime.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.

            //Runtime.UpdateFrequency = UpdateFrequency.None; // Disable self-execution.
            //Runtime.UpdateFrequency = UpdateFrequency.Once; // Run on the next tick, then remove.
            //Runtime.UpdateFrequence = UpdateFrequency.Update1; // Every tick.
            //Runtime.UpdateFrequency = UpdateFrequency.Update10; // Every 10th tick.
            Runtime.UpdateFrequency = UpdateFrequency.Update100; // Every 100th tick.
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from. Be aware that the
            // updateSource is a  bitfield  and might contain more than 
            // one update type.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.

            if (thisWaste == null)
                thisWaste = new MyWaste(this);

            if (myCnt > cMaxLoops)
            {
                thisWaste.Update();
                thisWaste.WasteIt();
                thisWaste.TextOutput(thisWaste.TextMessage);
                myCnt = 0;
            }
            else
            {
                myCnt = myCnt + 1;
            }
        }

        public class MyWaste
        {
            private int MaxStone;
            private int CurStone;
            private int MaxIce;
            private int CurIce;
            public string TextMessage { get; private set; }

            internal static Program ParentProgram;

            public MyWaste(Program MyProg)
            {
                ParentProgram = MyProg;
                MaxStone = (int)(ParentProgram.StoneDumpOn * ParentProgram.InvMulty);
                MaxIce = (int)(ParentProgram.IceDumpOn * ParentProgram.InvMulty);
                CurIce = 0;
                CurStone = 0;
                TextMessage = "";
            }

            public string InitilizeMe(string list)
            {
                return list + " run at "
                            + DateTime.Now.Hour.ToString("00") + ":"
                            + DateTime.Now.Minute.ToString("00") + ":"
                            + DateTime.Now.Second.ToString("00")
                            + "\n" + "------------------------------------------------------";
            }


            public void Update()
            {
                float perIce;
                float perStone;
                int InvPos = 0;
                List<IMyTerminalBlock> blockTemp = new List<IMyTerminalBlock>();
                List<MyInventoryItem> crateItems;
                ParentProgram.GridTerminalSystem.SearchBlocksOfName(ParentProgram.LookInPrefix, blockTemp);

                TextMessage = InitilizeMe("Waste System Tracking");
                TextMessage += "\n" + "Cargo Blocks: " + blockTemp.Count.ToString();
                CurIce = 0;
                CurStone = 0;
                for (int i = 0; i < blockTemp.Count; i++)
                {
                    // get the inventory and inventory items of the source container   
                    var SourceContainer = blockTemp[i];

                    if (SourceContainer == null) continue;
                    if (!SourceContainer.HasInventory) continue;

                    InvPos = SourceContainer.InventoryCount - 1;
                    var SourceInventory = SourceContainer.GetInventory(InvPos);
                    crateItems = new List<MyInventoryItem>();
                    SourceInventory.GetItems(crateItems);

                    if (crateItems.Count == 0) continue;

                    for (int j = crateItems.Count - 1; j >= 0; j--)
                    {
                        if (crateItems[j].Type.TypeId.ToString().Contains("Ore") && crateItems[j].Type.SubtypeId == "Stone")
                        {
                            CurStone += (int)crateItems[j].Amount;
                        }
                        else if (crateItems[j].Type.TypeId.ToString().Contains("Ore") && crateItems[j].Type.SubtypeId == "Ice")
                        {
                            CurIce += (int)crateItems[j].Amount;
                        }
                    }
                }
                perIce = MyPer(CurIce, MaxIce);
                perStone = MyPer(CurStone, MaxStone);
                TextMessage += "\n" + "Ice: " + CurIce.ToString("N0") + " = " + perIce.ToString("F2") + "%";
                TextMessage += "\n" + "Stone: " + CurStone.ToString("N0") + " = " + perStone.ToString("F2") + "%";
            }

            public float MyPer(int amt, int max)
            {
                int lvmax;
                float lvamt;
                lvmax = max;
                if (lvmax <= 0)
                {
                    lvmax = 1;
                }
                lvamt = (((float)amt / (float)lvmax) * 100f);
                //TextMessage += "\n" + lvamt.ToString() + " = " + amt.ToString() + " / " + lvmax.ToString() + " * 100 " ; 
                return lvamt;
            }


            public void WasteIt()
            {
                int IceWaste;
                int StoneWaste;
                //int TestAmount;
                int InvPos = 0;
                //float MoveAmt;
                List<MyInventoryItem> crateItems;

                IceWaste = CurIce - MaxIce;
                StoneWaste = CurStone - MaxStone;
                if (IceWaste > 0 || StoneWaste > 0)
                {
                    Turn("On");    // Turn Sorter Blocks On for the Move 

                    List<IMyTerminalBlock> blockFrom = new List<IMyTerminalBlock>();
                    ParentProgram.GridTerminalSystem.SearchBlocksOfName(ParentProgram.LookInPrefix, blockFrom);

                    List<IMyTerminalBlock> blockTo = new List<IMyTerminalBlock>();
                    ParentProgram.GridTerminalSystem.SearchBlocksOfName(ParentProgram.ContPrefix, blockTo);

                    if (blockTo.Count > 0)
                    {
                        for (int i = 0; i < blockFrom.Count; i++)
                        {

                            var SourceContainer = blockFrom[i];

                            if (SourceContainer == null) continue;
                            if (!SourceContainer.HasInventory) continue;

                            InvPos = SourceContainer.InventoryCount - 1;
                            var SourceInventory = SourceContainer.GetInventory(InvPos);
                            crateItems = new List<MyInventoryItem>();
                            SourceInventory.GetItems(crateItems);

                            if (crateItems.Count == 0) continue;

                                for (int j = crateItems.Count - 1; j >= 0; j--)
                                {
                                    if (crateItems[j].Type.TypeId.ToString().Contains("Ore") && crateItems[j].Type.SubtypeId == "Stone")
                                    {
                                        if (StoneWaste > 0)
                                        {
                                            StoneWaste = SendWaste(blockFrom[i], blockTo, StoneWaste, j, crateItems);
                                        }
                                    }
                                    else if (crateItems[j].Type.TypeId.ToString().Contains("Ore") && crateItems[j].Type.SubtypeId == "Ice")
                                    {
                                        if (IceWaste > 0)
                                        {
                                            IceWaste = SendWaste(blockFrom[i], blockTo, IceWaste, j, crateItems);
                                        }
                                    }
                                }
                            
                        }
                    }
                    else
                    {
                        TextMessage += "\n" + "* ERR * No Destinations";
                    }
                    Turn("Off");         // Turn Sorte Blocks OFF after move        } 
                }
            }

            private int SendWaste(IMyTerminalBlock FromCont, List<IMyTerminalBlock> ToCont, int Amount, int Item, List<MyInventoryItem> crateItems)
            {
                int RetAmt;
                int TestAmount;
                float MoveAmt;
                IMyInventory myFrom;
                IMyInventory myTo;
                string SourceItemName;

                RetAmt = Amount;

                if (!FromCont.HasInventory) return RetAmt;
                myFrom = FromCont.GetInventory(FromCont.InventoryCount - 1);
                SourceItemName = crateItems[Item].Type.SubtypeId;

                TestAmount = (int)crateItems[Item].Amount;
                if (TestAmount > RetAmt)
                    TestAmount = RetAmt;
                MoveAmt = (int)(TestAmount / ToCont.Count);
                for (int k = 0; k < ToCont.Count; k++)
                {
                    myTo = ToCont[k].GetInventory(ToCont[k].InventoryCount - 1);
                    if (!myFrom.TransferItemTo(myTo, Item, null, true, (VRage.MyFixedPoint)MoveAmt))
                    {
                        TextMessage += "\n" + "* ERR *: TRANSFER FAILED";
                        TextMessage += " ItemName: " + SourceItemName;
                        TextMessage += " From: " + FromCont.CustomName;
                        TextMessage += " To: " + ToCont[k].CustomName;
                        TextMessage += " Amount: " + MoveAmt.ToString();
                    }
                    else
                    {
                        RetAmt -= (int)MoveAmt;
                        // logging  
                        TextMessage += "\n" + SourceItemName;
                        TextMessage += " : " + FromCont.CustomName;
                        TextMessage += " >>> " + ToCont[k].CustomName;
                        TextMessage += " Amount: " + MoveAmt.ToString();
                    }
                }
                return RetAmt;
            }


            public void Turn(string OnOff)
            {
                List<IMyTerminalBlock> blockSort = new List<IMyTerminalBlock>();
                ParentProgram.GridTerminalSystem.SearchBlocksOfName(ParentProgram.SorterPrefix, blockSort);
                if (OnOff == "On")
                {
                    TextMessage += "\n" + "Sort Blocks: " + blockSort.Count.ToString();
                }
                for (int i = 0; i < blockSort.Count; i++)
                {
                    blockSort[i].GetActionWithName("OnOff_" + OnOff).Apply(blockSort[i]);
                }
            }

            public void TextOutput(string Output = "")
            {
                IMyTextPanel ScrObj = ParentProgram.GridTerminalSystem.GetBlockWithName(ParentProgram.LCD_Name) as IMyTextPanel;

                if (ScrObj != null)
                {
                    ScrObj.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
                    if (Output != "")
                    {
                        ScrObj.WriteText(Output);
                    }
                }
                else
                {
                    if (Output != "")
                    {
                        ParentProgram.Echo(Output);
                    }
                }
            }


        }


    }
}








