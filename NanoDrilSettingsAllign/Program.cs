﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // This file contains your actual script.
        //
        // You can either keep all your code here, or you can create separate
        // code files to make your program easier to navigate while coding.
        //
        // In order to add a new utility class, right-click on your project, 
        // select 'New' then 'Add Item...'. Now find the 'Space Engineers'
        // category under 'Visual C# Items' on the left hand side, and select
        // 'Utility Class' in the main area. Name it in the box below, and
        // press OK. This utility class will be merged in with your code when
        // deploying your final script.
        //
        // You can also simply create a new utility class manually, you don't
        // have to use the template if you don't want to. Just do so the first
        // time to see what a utility class looks like.
        // 
        // Go to:
        // https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
        //
        // to learn more about ingame scripts.
        // const string gNameDrills = "DrillSystem Base";
        const string bNameDrill = "xxxxxxx";
        const string cOffsetLR = "Drill.AreaOffsetLeftRight";   // X
        const string cOffsetUD = "Drill.AreaOffsetUpDown";      // Y
        const string cOffsetFB = "Drill.AreaOffsetFrontBack";   // Z
        const string cAreaW = "Drill.AreaWidth";
        const string cAreaH = "Drill.AreaHeight";
        const string cAreaD = "Drill.AreaDepth";
        const float myMult = 2.5f;
        //List<string> gNameDrills = new List<string>{ "DrillSystem Right", "DrillSystem Left" };
        List<string> gNameDrills = new List<string> { "DrillSystem" };
        const bool resetMe =     false;

        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set Runtime.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            string ERR_TXT = "";

            foreach (string gName in gNameDrills)
            {

                List<IMyTerminalBlock> DrilList = new List<IMyTerminalBlock>();
                if (GridTerminalSystem.GetBlockGroupWithName(gName) != null)
                {
                    GridTerminalSystem.GetBlockGroupWithName(gName).GetBlocksOfType<IMyFunctionalBlock>(DrilList, filterThis);
                    if (DrilList.Count == 0)
                    {
                        ERR_TXT += "group " + gName + " has no Drill blocks\n";
                    }
                }
                else
                {
                    ERR_TXT += "group" + gName + " not found\n";
                }

                Echo(ERR_TXT);

                bool myFirst;
                IMyTerminalBlock LinkToMe;

                Vector3I LocX = new Vector3I();
                Vector3I LocY = new Vector3I();
                Vector3I LocZ = new Vector3I();

                Single myLR1 = 0, myUD1 = 0, myFB1 = 0;
                Single myLR2 = 0, myUD2 = 0, myFB2 = 0;
                Single myLR3 = 0, myUD3 = 0, myFB3 = 0;
                Single myBSW = 0, myBSH = 0, myBSD = 0;

                LinkToMe = GridTerminalSystem.GetBlockWithName(bNameDrill);
                myFirst = true;
                if (LinkToMe != null)
                {
                    myFirst = false;
                    LocX = LinkToMe.Position;
                    myLR1 = LinkToMe.GetValue<Single>(cOffsetLR);
                    myUD1 = LinkToMe.GetValue<Single>(cOffsetUD);
                    myFB1 = LinkToMe.GetValue<Single>(cOffsetFB);
                    myBSW = LinkToMe.GetValue<Single>(cAreaW);
                    myBSH = LinkToMe.GetValue<Single>(cAreaH);
                    myBSD = LinkToMe.GetValue<Single>(cAreaD);
                    Echo(LinkToMe.CustomName);
                }


                foreach (IMyTerminalBlock item in DrilList)
                {
                    if (myFirst)
                    {
                        LinkToMe = item;
                        LocX = LinkToMe.Position;
                        myFirst = false;

                        myLR1 = LinkToMe.GetValue<Single>(cOffsetLR);
                        myUD1 = LinkToMe.GetValue<Single>(cOffsetUD);
                        myFB1 = LinkToMe.GetValue<Single>(cOffsetFB);
                        myBSW = LinkToMe.GetValue<Single>(cAreaW);
                        myBSH = LinkToMe.GetValue<Single>(cAreaH);
                        myBSD = LinkToMe.GetValue<Single>(cAreaD);
                        Echo(LinkToMe.CustomName);
                        if (resetMe)
                        {
                            myLR1 = 0;
                            myUD1 = 0;
                            myFB1 = 0;
                            myBSW = 75;
                            myBSH = 75;
                            myBSD = 75;
                            item.SetValue<Single>(cOffsetLR, myLR1);
                            item.SetValue<Single>(cOffsetUD, myUD1);
                            item.SetValue<Single>(cOffsetFB, myFB1);
                            item.SetValue<Single>(cAreaW, myBSW);
                            item.SetValue<Single>(cAreaH, myBSH);
                            item.SetValue<Single>(cAreaD, myBSD);
                        }

                    }
                    else
                    {

                        LocY = item.Position;
                        LocZ.X = LocX.X - LocY.X;
                        LocZ.Y = LocX.Y - LocY.Y;
                        LocZ.Z = LocX.Z - LocY.Z;

                        Echo("GPS Z: " + LocZ.X + ":" + LocZ.Y + ":" + LocZ.Z + " " + item.CustomName);

                        myLR2 = item.GetValue<Single>(cOffsetLR);
                        myUD2 = item.GetValue<Single>(cOffsetUD);
                        myFB2 = item.GetValue<Single>(cOffsetFB);
                        if (resetMe)
                        {
                            myLR3 = 0;
                            myUD3 = 0;
                            myFB3 = 0;
                        }
                        else
                        {
                            myLR3 = myLR1 + (LocZ.X * myMult);
                            myUD3 = myUD1 + (LocZ.Y * myMult);
                            myFB3 = myFB1 + (LocZ.Z * myMult);
                        }

                        item.SetValue<Single>(cOffsetLR, myLR3);
                        item.SetValue<Single>(cOffsetUD, myUD3);
                        item.SetValue<Single>(cOffsetFB, myFB3);
                        item.SetValue<Single>(cAreaW, myBSW);
                        item.SetValue<Single>(cAreaH, myBSH);
                        item.SetValue<Single>(cAreaD, myBSD);

                    }

                }
            }

        }

        bool filterThis(IMyTerminalBlock block)
        {
            return block.CubeGrid == Me.CubeGrid;
        }

    }
}
