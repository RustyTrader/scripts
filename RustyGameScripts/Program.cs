﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // This file contains your actual script.
        //
        // You can either keep all your code here, or you can create separate
        // code files to make your program easier to navigate while coding.
        //
        // In order to add a new utility class, right-click on your project, 
        // select 'New' then 'Add Item...'. Now find the 'Space Engineers'
        // category under 'Visual C# Items' on the left hand side, and select
        // 'Utility Class' in the main area. Name it in the box below, and
        // press OK. This utility class will be merged in with your code when
        // deploying your final script.
        //
        // You can also simply create a new utility class manually, you don't
        // have to use the template if you don't want to. Just do so the first
        // time to see what a utility class looks like.
        // 
        // Go to:
        // https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
        //
        // to learn more about ingame scripts.

        // Usage  
        // sort/LCDBaseIngot,LCDBaseOre,LCDBaseComp,LCDBaseGun,LCDBaseBottle  
        // StatusGen;  
        // http://girlsneaker.tumblr.com/page/70  
        // ------------------------------------------------------------   
        // LISTS   
        // ------------------------------------------------------------   
        // The Name of your LCD with your Lists. Mind, you can use one   
        // list for both purposes.   
        // Sortlists   
        // List<string> LCDNameSortList = new List<string>(new string[] { "LCD_Sort_Debug" });
        // List<string> LCDNameSortList = new List<string>(new string[] { "LCDInvIngot", "LCDInveOre" });   
        //   
        // Loadlists   
        List<string> LCDNameLoadList = new List<string>(new string[] { "LCD_Base_LoadList" });
        // List<string> LCDNameLoadList = new List<string>(new string[] { "LCDLoadWelder", "LCDLoadCargoVessel" });   

        // ------------------------------------------------------------   
        // EXCLUSION   
        // ------------------------------------------------------------   
        // Token for exclusion by containername   
        List<string> ExcludeTokens = new List<string>(new string[] { "#" });
        // Tag for your sorting-list, to exclude a number of containers   
        const string ExcludeTag = "Exclude";
        // Token für exclusion of graphical report   
        List<string> GraphExcludeTokens = new List<string>(new string[] { "#" });

        // ------------------------------------------------------------   
        // LOGGING   
        // ------------------------------------------------------------   
        // switch logging on LCD on (true) and off (false)   
        // if set to false, the logging output will be displayed right   
        // next to the programming block properties   
        Boolean UseDebugLCD = false;
        Boolean WriteOnceDebug = true;
        String DebugString = "";
        // LCD name for transfer information   
        List<string> LCDNameDebug = new List<string>(new string[] { "LCD_Sort_Debug" });
        //string LCDNameDebug = "LCD_Base_Debug";  

        // ------------------------------------------------------------   
        // GRAPH DISPLAYS   
        // ------------------------------------------------------------   
        // Display for general cargo status   
        List<string> LCDNameContainerStatus = new List<string>(new string[] { "LCD_Sort_Status_1", "LCD_Sort_Status_2", "LCD_Sort_Status_3" });
        //const string LCDNameContainerStatus = "LCD_Base_StatusCargo";  
        // Cargo Capacity Display width  
        const float cargoDispWidth = 100f;  // Default 30  
        const int maxLinesOnLCD = 19;       // Default 17
        const float fontsizeOnLCD = 0.9f;  // Default 1
        const bool doRefinerys = true;    // Must me Take ITems from Refinerys


        // ------------------------------------------------------------   
        // DIVIDERS   
        // ------------------------------------------------------------   
        // divider for the command with which the script is called   
        const char CommandDivider = '/';
        // divider for the load command   
        const char LoadTargetDivider = '/';
        // divider for the sort targets   
        const char SourceTargetDivider = ':';
        // general list divider   
        const char ListDivider = ',';
        // if the script is called with lists, this is the divider   
        const char ArgsListDivider = ',';
        const string CargoUsedSpace = "I";
        const string CargoEmptySpace = " ";

        // ------------------------------------------------------------   
        // CONTROLER
        // ------------------------------------------------------------   
        int ControllerSort = 0;
        bool SortDisplay = false;
        List<string> SortList;
        const int maxLoop = 10;
        int curLoop = 0;

        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set Runtime.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.

            SortDisplay = false;
            ControllerSort = 0;
            DebugMessage("", true);

            string Message = "";
            List<string> SortListName = new List<string>(LCDNameContainerStatus);

            // get sorting-list from LCD   
            for (int SortListIndex = 0; SortListIndex < SortListName.Count; SortListIndex++)
            {
                var LCDPanel = GridTerminalSystem.GetBlockWithName(SortListName[SortListIndex].Trim()) as IMyTextPanel;

                if (LCDPanel == null)
                {
                    DebugMessage("ERROR: LCD with sorter list not found!");
                    DebugMessage(SortListName[SortListIndex].Trim());
                    return;
                }

                Message += LCDPanel.CustomData + "\n";    //.GetPublicText() + "\n"; 
            }

            if (string.IsNullOrEmpty(Message))
            {
                DebugMessage("ERROR: LCD with sorter list is empty!");
                return;
            }

            SortList = new List<string>(Message.Split('\n'));
            //Runtime.UpdateFrequency = UpdateFrequency.None; // Disable self-execution.
            //Runtime.UpdateFrequency = UpdateFrequency.Once; // Run on the next tick, then remove.
            //Runtime.UpdateFrequence = UpdateFrequency.Update1; // Every tick.
            //Runtime.UpdateFrequency = UpdateFrequency.Update10; // Every 10th tick.
            Runtime.UpdateFrequency = UpdateFrequency.Update100; // Every 100th tick.
            curLoop = 0;
        }

        /// <summary>   
        /// Main function, start of the script   
        /// </summary>   
        /// <param name="Args"></param>   
        void Main(string argument, UpdateType updateType)
        {
            curLoop += 1;
            if (curLoop < maxLoop)
            {
                return;
            }
            curLoop = 0;
            List<string> LoadListName = new List<string>(LCDNameLoadList);

            string Command = "StatusSort";
            DebugMessage("* Start Sort *", true);

            if (!string.IsNullOrEmpty(argument))
            {
                List<string> ArgsList = new List<string>(argument.Split(CommandDivider));

                if (ArgsList.Count > 0)
                    Command = argument.Split(CommandDivider)[0].Trim();
            }
            else
            {
                DebugMessage("Using all defaults!");
            }

            switch (Command)
            {
                case "sort":
                case "Sort":
                    Sorter();
                    break;
                case "load":
                case "Load":
                    Loader(LoadListName);
                    break;
                case "statusgen":
                case "StatusGen":
                    GraphicContainerStatus();
                    break;
                case "statussort":
                case "StatusSort":
                    if (SortDisplay)
                    {
                        DebugMessage("StatusSort Display");
                        GraphicContainerStatus();
                        SortDisplay = false;
                    }
                    else 
                    {
                        DebugMessage("StatusSort Sort");
                        Sorter();
                        SortDisplay = true;
                    }
                    break;
                default:
                    DebugMessage("ERROR: Unknown command!");
                    break;
            }

            DebugMessage("Run successful completed", false, true);
        }

        /// <summary>   
        /// helper function, to have simpler logging commands   
        /// </summary>   
        /// <param name="Message">message content</param>   
        void DebugMessage(string Message)
        {
            DebugMessage(Message, false);
        }
        void DebugMessage(string Message, bool Clean)
        {
            DebugMessage(Message, Clean, false);
        }
        void DebugMessage(string Message, bool Clean, bool Final)
        {
            if (UseDebugLCD)
            {
                if (Clean == true)
                {
                    DebugString = "";
                }
                if (Final == true ^ WriteOnceDebug == false)
                {
                    DisplayOnLCD(LCDNameDebug, DebugString + "\n" + Message, WriteOnceDebug);
                }
                else
                {
                    if (DebugString != "")
                    {
                        DebugString = DebugString + "\n";
                    }
                    DebugString = DebugString + Message;
                }
            }
            else
            {
                Echo(Message);
            }
        }

        /// <summary>   
        /// helper function, to put text on a LCD   
        /// </summary>   
        /// <param name="LCDName"></param>   
        /// <param name="Content"></param>   
        void DisplayOnLCD(List<string> LCDName, string Content, bool Clean)
        {
            if (LCDName.Count == 0) return;

            string OldText = "";
            string NewText = "";
            int WorkCnt1 = 0;
            int WorkCnt2 = 0;
            bool firstLine = true;
            StringBuilder inText = new StringBuilder();

            for (int LCDNameIndex = 0; LCDNameIndex < LCDName.Count; LCDNameIndex++)
            {
                var LCDPanel = GridTerminalSystem.GetBlockWithName(LCDName[LCDNameIndex].Trim()) as IMyTextPanel;

                if (LCDPanel == null)
                {
                    DebugMessage("ERROR: LCD " + LCDName[LCDNameIndex].Trim() + " not found!");
                    return;
                }
                else
                {
                    LCDPanel.FontSize = fontsizeOnLCD;
                    LCDPanel.ReadText(inText);
                    NewText = inText.ToString().Trim();
                    if (LCDNameIndex > 0)
                    {
                        if (NewText != "" && OldText.Trim() != "")
                        {
                            NewText = "\n" + NewText;
                        }
                    }
                    OldText += NewText;
                }
            }

            if (Clean)
            {
                NewText = Content;
            }
            else
            {
                NewText = OldText + "\n" + Content;
            }

            List<string> DispLines = new List<string>(NewText.Split('\n'));

            WorkCnt1 = 0;
            WorkCnt2 = 0;

            for (int LCDNameIndex = 0; LCDNameIndex < LCDName.Count; LCDNameIndex++)
            {
                var LCDPanel = GridTerminalSystem.GetBlockWithName(LCDName[LCDNameIndex].Trim()) as IMyTextPanel;

                if (LCDPanel == null)
                {
                    DebugMessage("ERROR: LCD " + LCDName[LCDNameIndex].Trim() + " not found!");
                    return;
                }
                else
                {
                    if (LCDNameIndex < LCDName.Count - 1)
                    {
                        WorkCnt1 = WorkCnt2;
                        WorkCnt2 = WorkCnt1 + maxLinesOnLCD;
                        if (WorkCnt2 > DispLines.Count) WorkCnt2 = DispLines.Count;
                    }
                    else
                    {
                        WorkCnt1 = WorkCnt2;
                        WorkCnt2 = DispLines.Count;
                    }
                    NewText = "";
                    if (WorkCnt1 < WorkCnt2)
                    {
                        firstLine = true;
                        for (int LineIndex = WorkCnt1; LineIndex < WorkCnt2; LineIndex++)
                        {
                            if (!firstLine)
                            {
                                NewText += "\n";
                            }
                            else
                            {
                                firstLine = false;
                            }
                            NewText += DispLines[LineIndex].Trim();
                        }
                    }
                    else
                    {
                        NewText = " ";
                    }
                    LCDPanel.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
                    LCDPanel.WriteText(NewText);
                }
            }

            return;
        }

        /// <summary>   
        /// helper function, to get all source blocks with inventories   
        /// for the sorter   
        /// </summary>   
        /// <returns></returns>   
        List<IMyTerminalBlock> InitializeSources()
        {
            // get all cargo-containers in the grid   
            List<IMyTerminalBlock> Containers = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(Containers);

            // get all refineries in the grid   
            // arc furnaces are refineries   
            List<IMyTerminalBlock> Refineries = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyRefinery>(Refineries);
            Containers.AddList<IMyTerminalBlock>(Refineries);

            // get all assemblers in the grid   
            List<IMyTerminalBlock> Assembler = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyAssembler>(Assembler);
            Containers.AddList<IMyTerminalBlock>(Assembler);

            // get all connectors in the grid   
            List<IMyTerminalBlock> Connector = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(Connector);
            Containers.AddList<IMyTerminalBlock>(Connector);

            return Containers;
        }

        /// <summary>   
        /// helper, to initialize the logging   
        /// </summary>   
        /// <param name="Headline"></param>   
        /// <returns></returns>   
        string InitializeOutput(string Headline)
        {
            string Output = Headline + " run at "
                    + DateTime.Now.Hour.ToString("00") + ":"
                    + DateTime.Now.Minute.ToString("00") + ":"
                    + DateTime.Now.Second.ToString("00")
                    + "\n  ----------------------------------------";
            return Output;
        }

        /// <summary>   
        /// the loader function enables you, to place a specific amount of   
        /// an item into a specific container   
        /// </summary>   
        /// <param name="SortListName"></param>   
        /// <param name="LoadListName"></param>   
        void Loader(List<string> LoadListName)
        {
            string Output = InitializeOutput("Loader");
            DebugMessage(Output);

            string LoadMessage = "";

            // get loader list from LCD   
            for (int LoadListIndex = 0; LoadListIndex < LoadListName.Count; LoadListIndex++)
            {
                var LCDPanel = GridTerminalSystem.GetBlockWithName(LoadListName[LoadListIndex].Trim()) as IMyTextPanel;

                if (LCDPanel == null)
                {
                    DebugMessage("ERROR: LCD with loader list not found!");
                    return;
                }

                LoadMessage += "\n" + LCDPanel.CustomData + "\n";
            }

            if (string.IsNullOrEmpty(LoadMessage))
            {
                DebugMessage("ERROR: LCD with loader list is empty!");
                return;
            }

            List<IMyTerminalBlock> Containers = InitializeSources();

            if (Containers.Count == 0)
            {
                DebugMessage("ERROR: No source blocks found!");
                return;
            }

            List<string> LoadList = new List<string>(LoadMessage.Split('\n'));
            List<MyInventoryItem> SourceItems;
            int myTrf = 0;
            myTrf = 0;
            for (int LoadIndex = 0; LoadIndex < LoadList.Count; LoadIndex++)
            {
                if (myTrf > 10) break;

                List<string> LoadArgs = new List<string>(LoadList[LoadIndex].Split(LoadTargetDivider));

                if (LoadArgs.Count < 4) continue;

                string Amount = LoadArgs[2];
                string TargetName = LoadArgs[3].Trim();

                if (string.IsNullOrEmpty(Amount) || string.IsNullOrEmpty(TargetName)) continue;

                string TypeName = LoadArgs[0] + "/" + LoadArgs[1];
                VRage.MyFixedPoint Quantity = 0;

                try
                {
                    Quantity = Convert.ToInt32(Amount);
                }
                catch
                {
                    DebugMessage("ERROR: " + Amount + " is not a valid amount!");
                    continue;
                }

                var TargetContainer = GridTerminalSystem.GetBlockWithName(TargetName);

                if (TargetContainer == null || !TargetContainer.HasInventory) continue;

                List<string> SourceDefinitionList = SortList.FindAll(x => x.StartsWith(TypeName));

                for (int DefinitionIndex = 0; DefinitionIndex < SourceDefinitionList.Count; DefinitionIndex++)
                {
                    if (myTrf > 10) break;

                    if (!SourceDefinitionList[DefinitionIndex].Contains(SourceTargetDivider.ToString())) continue;

                    var Sources = SourceDefinitionList[DefinitionIndex].Split(SourceTargetDivider)[1].Trim();
                    List<string> SortTargets = new List<string>();

                    List<string> SourceContainerList = new List<string>();

                    // check for multiple targets   
                    if (Sources.Contains(ListDivider.ToString()))
                    {
                        SourceContainerList.AddArray<string>(Sources.Split(ListDivider));
                    }
                    else
                    {
                        SourceContainerList.Add(Sources);
                    }

                    for (int SourceIndex = 0; SourceIndex < SourceContainerList.Count; SourceIndex++)
                    {
                        if (myTrf > 10) break;
                        string SourceName = SourceContainerList[SourceIndex];
                        var SourceContainer = GridTerminalSystem.GetBlockWithName(SourceName);

                        if (SourceContainer == null || TargetContainer == SourceContainer || !SourceContainer.HasInventory) continue;

                        IMyInventory TargetInventory = TargetContainer.GetInventory(TargetContainer.InventoryCount - 1);
                        VRage.MyFixedPoint VolumeFull = TargetInventory.MaxVolume * (VRage.MyFixedPoint)0.97;
                        VRage.MyFixedPoint CurrentTargetAmount = GetItemQuantityFromInventory(TargetInventory, TypeName);
                        VRage.MyFixedPoint TransferQuantity = Quantity - CurrentTargetAmount;

                        if (TransferQuantity <= 0) break;
                        if (TargetInventory.CurrentVolume >= VolumeFull)
                        {
                            DebugMessage("Target " + TargetName + " is over 97% full!");
                            break;
                        }

                        IMyInventory SourceInventory = SourceContainer.GetInventory(SourceContainer.InventoryCount - 1);

                        if (!SourceInventory.IsConnectedTo(TargetInventory)) continue;
                        if (GetItemQuantityFromInventory(SourceInventory, TypeName) == 0) continue;

                        SourceItems = new List<MyInventoryItem>();
                        SourceInventory.GetItems(SourceItems);
                        int SourceItemIndex = 0;
                        Boolean FoundSourceItem = false;

                        if (SourceItems.Count > 1)
                        {
                            for (SourceItemIndex = 0; SourceItemIndex < SourceItems.Count; SourceItemIndex++)
                            {
                                if (GetInventoryItemName(SourceItems[SourceItemIndex]) == TypeName)
                                {
                                    FoundSourceItem = true;
                                    break;
                                }
                            }
                        }

                        if (!FoundSourceItem)
                        {
                            DebugMessage("Target " + TargetName + " doesn't contain " + TypeName);
                            break;
                        }

                        if (!SourceInventory.TransferItemTo(TargetInventory, SourceItemIndex, null, true, TransferQuantity))
                        {
                            DebugMessage("ERROR: ITEM TRANSFER FAILED");
                            DebugMessage("SourceItemName: " + TypeName);
                            DebugMessage("SourceContainer: " + SourceContainer.CustomName);
                            DebugMessage("TargetContainer: " + TargetContainer.CustomName);
                            DebugMessage("Quantity: " + TransferQuantity.ToString());
                            break;
                        }
                        DebugMessage(TypeName +
                                " : " + SourceContainer.CustomName +
                                " >>> " + TransferQuantity.ToString() +
                                " >>> " + TargetContainer.CustomName);
                        myTrf += 1;
                        VRage.MyFixedPoint TestTargetAmount = GetItemQuantityFromInventory(TargetInventory, TypeName);

                        if (TestTargetAmount >= Quantity) break;
                        if (TargetInventory.CurrentVolume >= VolumeFull) break;
                    }
                }
            }
        }

        /// <summary>   
        /// helpfer function, to calculate the amount of an item   
        /// in a container   
        /// </summary>   
        /// <param name="TargetInventory"></param>   
        /// <param name="TypeName"></param>   
        /// <returns></returns>   
        VRage.MyFixedPoint GetItemQuantityFromInventory(IMyInventory TargetInventory, string TypeName)
        {
            List<MyInventoryItem> TargetItems = new List<MyInventoryItem>();
            VRage.MyFixedPoint Quantity = 0;
            TargetInventory.GetItems(TargetItems);

            if (TargetInventory.CurrentMass > 0)
            {
                for (int TargetItemIndex = 0; TargetItemIndex < TargetItems.Count; TargetItemIndex++)
                {
                    var TargetItem = TargetItems[TargetItemIndex];

                    if (GetInventoryItemName(TargetItem) == TypeName)
                        Quantity += TargetItem.Amount;
                }
            }

            return Quantity;
        }


        /// <summary>   
        /// the sorter script sorts the items to containers, defined on you   
        /// sorting lists   
        /// </summary>   
        /// <param name="SortListName"></param>   
        void Sorter()
        {
            switch (ControllerSort)
            {
                case 1:
                    SorterNorm();
                    ControllerSort = 2;
                    break;
                case 2:
                    SorterAsem();
                    ControllerSort = 1;
                    break;
                default:
                    ControllerSort = 1;
                    break;
            }
        }

        /// <summary>   
        /// the sorter script sorts the items to containers, defined on you   
        /// sorting lists   
        /// </summary>   
        /// <param name="SortListName"></param>   
        void SorterNorm()
        {
            string Output = InitializeOutput("Sorter");
            int InvPos = 0;
            DebugMessage(Output);

            List<IMyTerminalBlock> Containers = InitializeSources();

            if (Containers.Count == 0)
            {
                DebugMessage("ERROR: No source blocks found!");
                return;
            }

            // make lists off the LCD-input   
//            List<string> SortList = new List<string>(Message.Split('\n'));
            List<string> SortItemNames = new List<string>();
            List<string> SortTargets = new List<string>();
            List<string> Excludes = new List<string>();

            //List for the Item not Caterd for in the Sort Lists  
            List<string> UnknownList = new List<string>();

            // refine the list   
            for (int SortIndex = 0; SortIndex < SortList.Count; SortIndex++)
            {
                if (!SortList[SortIndex].Contains(SourceTargetDivider.ToString())) continue;

                var ItemName = SortList[SortIndex].Split(SourceTargetDivider)[0].Trim();
                var Targets = SortList[SortIndex].Split(SourceTargetDivider)[1].Trim();

                if (ItemName == ExcludeTag)
                {
                    if (Targets.Contains(ListDivider.ToString()))
                    {
                        Excludes.AddArray<string>(Targets.Split(ListDivider));
                    }
                    else
                    {
                        Excludes.Add(Targets);
                    }
                    continue;
                }

                SortItemNames.Add(ItemName);
                SortTargets.Add(Targets);
            }

            List<MyInventoryItem> SourceItems;
            List<MyInventoryItem> TestItems;

            // the outer loop goes through all containers, assembler, refineries and connectors   
            for (int SourceContainerIndex = 0; SourceContainerIndex < Containers.Count; SourceContainerIndex++)
            {
                // get the inventory and inventory items of the source container   
                var SourceContainer = Containers[SourceContainerIndex];

                if (Excludes.Contains(SourceContainer.CustomName)) continue;

                //if (SourceContainer.GetType().Name == "MyAssembler" && (SourceContainer as IMyAssembler).Mode == MyAssemblerMode.Disassembly) continue;
                if (SourceContainer.GetType().Name == "MyRefinery" && !doRefinerys) continue;

                InvPos = SourceContainer.InventoryCount - 1;
                if (SourceContainer.GetType().Name == "MyAssembler" && (SourceContainer as IMyAssembler).Mode == MyAssemblerMode.Disassembly)
                {
                    InvPos = 0;
                }

                Boolean IsExclude = false;

                for (int ExcludeIndex = 0; ExcludeIndex < ExcludeTokens.Count; ExcludeIndex++)
                {
                    if (SourceContainer.CustomName.StartsWith(ExcludeTokens[ExcludeIndex]) || SourceContainer.CustomName.EndsWith(ExcludeTokens[ExcludeIndex]))
                    {
                        IsExclude = true;
                        break;
                    }
                }

                if (IsExclude) continue;
                if (!SourceContainer.HasInventory) continue;

                Echo($"{SourceContainer.GetType().Name } > ");

                var SourceInventory = SourceContainer.GetInventory(InvPos);
                SourceItems = new List<MyInventoryItem>();
                SourceInventory.GetItems(SourceItems);

                if (SourceItems.Count == 0) continue;

                // the first inner loop goes through all inventory items in the source inventory   
                // it will be skipped, if there are none - hence the while-loop   
                int SourceItemIndex = 0;

                while (SourceItemIndex < SourceItems.Count)
                {
                    // get an item from the source-inventory   
                    var SourceItem = SourceItems[SourceItemIndex];
                    // get the name of the item   
                    var SourceItemName = GetInventoryItemName(SourceItem);

                    // check, if the item is on the sorting-list   
                    int Index = SortItemNames.IndexOf(SourceItemName);
                    // if not, go to the next item   
                    if (Index == -1)
                    {
                        Index = UnknownList.IndexOf(SourceItemName);
                        if (Index == -1)
                        {
                            UnknownList.Add(SourceItemName);
                        }
                        SourceItemIndex++;
                        continue;
                    }

                    // at this point we have an item, which is on our sorting list   

                    // if no targets are defined, skip this iteration   
                    if (SortTargets[Index] == null || SortTargets[Index] == "")
                    {
                        SourceItemIndex++;
                        continue;
                    }

                    // get the targets for this item   
                    List<string> TargetContainerList = new List<string>();

                    // check for multiple targets   
                    if (SortTargets[Index].Contains(ListDivider.ToString()))
                    {
                        TargetContainerList.AddArray<string>(SortTargets[Index].Split(ListDivider));
                    }
                    else
                    {
                        TargetContainerList.Add(SortTargets[Index]);
                    }
                    if (SortTargets[Index].Contains(SourceContainer.CustomName))
                    {
                        SourceItemIndex++;
                        continue;
                    }

                    // iterate through the targets   
                    for (int TargetIndex = 0; TargetIndex < TargetContainerList.Count; TargetIndex++)
                    {
                        // get the target name   
                        string TargetName = TargetContainerList[TargetIndex];
                        // get the target container   
                        var TargetContainer = GridTerminalSystem.GetBlockWithName(TargetName);

                        if (TargetContainer == null || TargetContainer == SourceContainer || !TargetContainer.HasInventory) continue;

                        IMyInventory TargetInventory = TargetContainer.GetInventory(TargetContainer.InventoryCount - 1);
                        VRage.MyFixedPoint VolumeFull = TargetInventory.MaxVolume * (VRage.MyFixedPoint)0.95;

                        if (!SourceInventory.IsConnectedTo(TargetInventory) ||
                            TargetInventory.CurrentVolume >= VolumeFull) continue;

                        // handle the transfer   
                        if (!SourceInventory.TransferItemTo(TargetInventory, SourceItemIndex, null, true, null))
                        {
                            DebugMessage("ERROR: ITEM TRANSFER FAILED");
                            DebugMessage("SourceItemName: " + SourceItemName);
                            DebugMessage("SourceContainer: " + SourceContainer.CustomName);
                            DebugMessage("TargetContainer: " + TargetContainerList[TargetIndex]);
                            break;
                        }
                        // logging   
                        DebugMessage(SourceItemName +
                                " : " + SourceContainer.CustomName +
                                " >>> " + TargetContainerList[TargetIndex]);

                        if (TargetInventory.CurrentVolume < VolumeFull) break;
                        // check if there is something left in the sourcecontainer   
                        var TestInventory = SourceContainer.GetInventory(SourceContainer.InventoryCount - 1);
                        TestItems = new List<MyInventoryItem>();
                        TestInventory.GetItems(TestItems);

                        if (TestItems.Count == 0 || TestItems.Count <= SourceItemIndex) break;
                        if (GetInventoryItemName(TestItems[SourceItemIndex]) != SourceItemName) break;
                    }
                    SourceItemIndex++;
                }
            }
            if (UnknownList.Count > 0)
            {
                DebugMessage("\n Unknown Items \n ----------------------");
                for (int ListIndex = 0; ListIndex < UnknownList.Count; ListIndex++)
                {
                    DebugMessage(UnknownList[ListIndex]);
                }
            }
            UnknownList.Clear();
        }


        /// <summary>   
        /// the sorter script sorts the items to containers, defined on you   
        /// sorting lists   
        /// </summary>   
        /// <param name="SortListName"></param>   
        void SorterAsem()
        {
            string Output = InitializeOutput("Sorter");
            int InvPos = 0;
            DebugMessage(Output);


            List<IMyTerminalBlock> Containers = InitializeSources();

            if (Containers.Count == 0)
            {
                DebugMessage("ERROR: No source blocks found!");
                return;
            }

            // make lists off the LCD-input   
            List<string> SortItemNames = new List<string>();
            List<string> SortTargets = new List<string>();
            List<string> Excludes = new List<string>();

            //List for the Item not Caterd for in the Sort Lists  
            List<string> UnknownList = new List<string>();

            // refine the list   
            for (int SortIndex = 0; SortIndex < SortList.Count; SortIndex++)
            {
                if (!SortList[SortIndex].Contains(SourceTargetDivider.ToString())) continue;

                var ItemName = SortList[SortIndex].Split(SourceTargetDivider)[0].Trim();
                var Targets = SortList[SortIndex].Split(SourceTargetDivider)[1].Trim();

                if (ItemName == ExcludeTag)
                {
                    if (Targets.Contains(ListDivider.ToString()))
                    {
                        Excludes.AddArray<string>(Targets.Split(ListDivider));
                    }
                    else
                    {
                        Excludes.Add(Targets);
                    }
                    continue;
                }

                SortItemNames.Add(ItemName);
                SortTargets.Add(Targets);
            }

            List<MyInventoryItem> SourceItems;
            List<MyInventoryItem> TestItems;

            // the outer loop goes through all containers, assembler, refineries and connectors   
            for (int SourceContainerIndex = 0; SourceContainerIndex < Containers.Count; SourceContainerIndex++)
            {
                // get the inventory and inventory items of the source container   
                var SourceContainer = Containers[SourceContainerIndex];

                if (Excludes.Contains(SourceContainer.CustomName)) continue;
                // We Only want Assemblers
                if (SourceContainer.GetType().Name != "MyAssembler") continue;
                InvPos = 0;

                Boolean IsExclude = false;

                for (int ExcludeIndex = 0; ExcludeIndex < ExcludeTokens.Count; ExcludeIndex++)
                {
                    if (SourceContainer.CustomName.StartsWith(ExcludeTokens[ExcludeIndex]) || SourceContainer.CustomName.EndsWith(ExcludeTokens[ExcludeIndex]))
                    {
                        IsExclude = true;
                        break;
                    }
                }

                if (IsExclude) continue;
                if (!SourceContainer.HasInventory) continue;

                Echo($"{SourceContainer.GetType().Name } > ");

                var SourceInventory = SourceContainer.GetInventory(InvPos);
                SourceItems = new List<MyInventoryItem>();
                SourceInventory.GetItems(SourceItems);

                if (SourceItems.Count == 0) continue;
                MyFixedPoint TrfAmt;

                // the first inner loop goes through all inventory items in the source inventory   
                // it will be skipped, if there are none - hence the while-loop   
                int SourceItemIndex = 0;

                while (SourceItemIndex < SourceItems.Count)
                {
                    // get an item from the source-inventory   
                    var SourceItem = SourceItems[SourceItemIndex];
                    TrfAmt = SourceItem.Amount;
                    if (TrfAmt < 1000)
                    {
                        SourceItemIndex++;
                        continue;
                    }

                    // get the name of the item   
                    var SourceItemName = GetInventoryItemName(SourceItem);

                    // check, if the item is on the sorting-list   
                    int Index = SortItemNames.IndexOf(SourceItemName);
                    // if not, go to the next item   
                    if (Index == -1)
                    {
                        Index = UnknownList.IndexOf(SourceItemName);
                        if (Index == -1)
                        {
                            UnknownList.Add(SourceItemName);
                        }
                        SourceItemIndex++;
                        continue;
                    }

                    // at this point we have an item, which is on our sorting list   

                    // if no targets are defined, skip this iteration   
                    if (SortTargets[Index] == null || SortTargets[Index] == "")
                    {
                        SourceItemIndex++;
                        continue;
                    }

                    // get the targets for this item   
                    List<string> TargetContainerList = new List<string>();

                    // check for multiple targets   
                    if (SortTargets[Index].Contains(ListDivider.ToString()))
                    {
                        TargetContainerList.AddArray<string>(SortTargets[Index].Split(ListDivider));
                    }
                    else
                    {
                        TargetContainerList.Add(SortTargets[Index]);
                    }
                    if (SortTargets[Index].Contains(SourceContainer.CustomName))
                    {
                        SourceItemIndex++;
                        continue;
                    }


                    // iterate through the targets   
                    for (int TargetIndex = 0; TargetIndex < TargetContainerList.Count; TargetIndex++)
                    {
                        // get the target name   
                        string TargetName = TargetContainerList[TargetIndex];
                        // get the target container   
                        var TargetContainer = GridTerminalSystem.GetBlockWithName(TargetName);

                        if (TargetContainer == null || TargetContainer == SourceContainer || !TargetContainer.HasInventory) continue;

                        IMyInventory TargetInventory = TargetContainer.GetInventory(TargetContainer.InventoryCount - 1);
                        VRage.MyFixedPoint VolumeFull = TargetInventory.MaxVolume * (VRage.MyFixedPoint)0.95;

                        if (!SourceInventory.IsConnectedTo(TargetInventory) ||
                            TargetInventory.CurrentVolume >= VolumeFull) continue;

                        TrfAmt -= 1000;
                        // handle the transfer   
                        if (!SourceInventory.TransferItemTo(TargetInventory, SourceItemIndex, null, true, TrfAmt))
                        {
                            DebugMessage("ERROR: ITEM TRANSFER FAILED");
                            DebugMessage("SourceItemName: " + SourceItemName);
                            DebugMessage("SourceContainer: " + SourceContainer.CustomName);
                            DebugMessage("TargetContainer: " + TargetContainerList[TargetIndex]);
                            break;
                        }
                        // logging   
                        DebugMessage(SourceItemName +
                                " : " + SourceContainer.CustomName +
                                " >>> " + TargetContainerList[TargetIndex]);

                        if (TargetInventory.CurrentVolume < VolumeFull) break;
                        // check if there is something left in the sourcecontainer   
                        var TestInventory = SourceContainer.GetInventory(SourceContainer.InventoryCount - 1);
                        TestItems = new List<MyInventoryItem>();
                        TestInventory.GetItems(TestItems);

                        if (TestItems.Count == 0 || TestItems.Count <= SourceItemIndex) break;
                        if (GetInventoryItemName(TestItems[SourceItemIndex]) != SourceItemName) break;
                    }
                    SourceItemIndex++;
                }
            }
            if (UnknownList.Count > 0)
            {
                DebugMessage("\n Unknown Items \n ----------------------");
                for (int ListIndex = 0; ListIndex < UnknownList.Count; ListIndex++)
                {
                    DebugMessage(UnknownList[ListIndex]);
                }
            }
            UnknownList.Clear();
        }


        /// <summary>   
        /// returns the name of an item as Maintype/Subtype   
        /// for example: "Ore/Iron"   
        /// </summary>   
        /// <param name="item"></param>   
        /// <returns></returns>   
        string GetInventoryItemName(MyInventoryItem item)
        {
            return item.Type.TypeId.ToString().Split('_')[1] + "/" + item.Type.SubtypeId;
        }

        /// <summary>   
        /// Simple graphical display of the current cargo status   
        /// </summary>   
        void GraphicContainerStatus()
        {
            string Output = InitializeOutput("General Container Status");
            //DisplayOnLCD(LCDNameContainerStatus, "  " + Output, true);
            Output = "\n";  /// "\n";

            List<IMyTerminalBlock> Containers = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(Containers);
            if (Containers.Count == 0) return;

            Containers.Sort((x, y) => x.CustomName.CompareTo(y.CustomName));

            for (int SourceContainerIndex = 0; SourceContainerIndex < Containers.Count; SourceContainerIndex++)
            {
                var SourceContainer = Containers[SourceContainerIndex];
                Boolean IsExclude = true;

                if (SourceContainer.CustomName.StartsWith("LCargo"))
                { IsExclude = false; }
                if (SourceContainer.CustomName.StartsWith("SCargo"))
                { IsExclude = false; }

                for (int ExcludeIndex = 0; ExcludeIndex < GraphExcludeTokens.Count; ExcludeIndex++)
                {
                    if (SourceContainer.CustomName.StartsWith(GraphExcludeTokens[ExcludeIndex]))
                    {
                        IsExclude = true;
                        break;
                    }
                }

                if (IsExclude) continue;

                var SourceInventory = SourceContainer.GetInventory(SourceContainer.InventoryCount - 1);

                Output += "  [";

                double Volume = cargoDispWidth / double.Parse(SourceInventory.MaxVolume.ToString()) * double.Parse(SourceInventory.CurrentVolume.ToString());

                for (int i = 0; i < (int)cargoDispWidth; i++)
                {
                    if (i < Volume) Output += CargoUsedSpace;
                    else Output += CargoEmptySpace;
                }

                Output += "]  " + SourceContainer.CustomName + "\n";
            }
            DisplayOnLCD(LCDNameContainerStatus, Output, true);  // false);
            return;
        }
    }
}
