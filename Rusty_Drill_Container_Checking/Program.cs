﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        enum myStateControl
        {
            On = 1,
            Auto = 2,
            Off = 3
        }

        const string gNameDrills = "Drills (Control)";
        const string gNameContainers = "CargoContainer (Control)";
        const string gNameStoneStore = "LCargoStone (Control)";
        const string gNameIceStore = "LCargoIce (Control)";
        List<string> myOutScreen = new List<string> { "ProgLCDDrill_L", "ProgLCDDrill_R" };
        const int myDispWid = 20;
        const int myExtraCargo = 4;   // Extr Display Width for Cargo on Left
        const float perAutoOn = 40f;
        const float perAutoOff = 98f;
        const int myLinesPerScr = 10;   // Nomber of Lines for Container Status per 1st page
        const float myFontSizeCargo = 1.4f;    // Fontsize for Cargo Status Display
        myStateControl myDrillControl = myStateControl.Off;
        List<IMyTerminalBlock> myStoneList;
        List<IMyTerminalBlock> myIceList;

        bool myDrillState = false;


        public Program()
        {
            // It's recommended to set Runtime.UpdateFrequency
            // here, which will allow your script to run itself without a
            // timer block.
            //Runtime.UpdateFrequency = UpdateFrequency.None; // Disable self-execution.
            //Runtime.UpdateFrequency = UpdateFrequency.Once; // Run on the next tick, then remove.
            //Runtime.UpdateFrequence = UpdateFrequency.Update1; // Every tick.
            //Runtime.UpdateFrequency = UpdateFrequency.Update10; // Every 10th tick.
            Runtime.UpdateFrequency = UpdateFrequency.Update100; // Every 100th tick.

            IMyTextSurface mesurface1 = Me.GetSurface(1);
            mesurface1.ContentType = ContentType.TEXT_AND_IMAGE;
            StringBuilder myStrBuff = new StringBuilder();
            mesurface1.ReadText(myStrBuff);

            List<string> ArgsList = new List<string>(myStrBuff.ToString().Split('\n'));

            myStoneList = new List<IMyTerminalBlock>();
            myIceList = new List<IMyTerminalBlock>();

            string myStat = "";

            if (ArgsList.Count > 0)
            {
                myStat = ArgsList[0].Trim();
            }

            if (!string.IsNullOrEmpty(myStat))
            {
                if (!Enum.TryParse(myStat, out myDrillControl)) myDrillControl = myStateControl.Off;
            }

            if (GridTerminalSystem.GetBlockGroupWithName(gNameStoneStore) != null)
            {
                GridTerminalSystem.GetBlockGroupWithName(gNameStoneStore).GetBlocksOfType<IMyTerminalBlock>(myStoneList, filterThis);
                if (myStoneList.Count == 0)
                {
                    Echo("group " + gNameStoneStore + " has no blocks");
                }
            }

            if (GridTerminalSystem.GetBlockGroupWithName(gNameIceStore) != null)
            {
                GridTerminalSystem.GetBlockGroupWithName(gNameIceStore).GetBlocksOfType<IMyTerminalBlock>(myIceList, filterThis);
                if (myIceList.Count == 0)
                {
                    Echo("group " + gNameIceStore + " has no blocks");
                }
            }

            if( !myOutScreen.Contains(Me.CustomName.Trim()))
            {
                myOutScreen.Add(Me.CustomName.Trim());
            }

        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means.
            //
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The method itself is required, but the arguments above
            // can be removed if not needed.
            // block declarations
            string ERR_TXT = "";
            string myMess = "";
            string myMess0 = "";
            string myMess3 = "";
            string myMess4 = "";

            List<IMyTerminalBlock> l0 = new List<IMyTerminalBlock>();
            if (GridTerminalSystem.GetBlockGroupWithName(gNameContainers) != null)
            {
                GridTerminalSystem.GetBlockGroupWithName(gNameContainers).GetBlocksOfType<IMyCargoContainer>(l0, filterThis);
                if (l0.Count == 0)
                {
                    ERR_TXT += "group " + gNameContainers + " has no Container blocks\n";
                }
            }
            else
            {
                ERR_TXT += "group" + gNameContainers + " not found\n";
            }

            l0.Sort((x, y) => x.CustomName.CompareTo(y.CustomName));


            List<IMyTerminalBlock> l2 = new List<IMyTerminalBlock>();
            if (GridTerminalSystem.GetBlockGroupWithName(gNameDrills) != null)
            {
                GridTerminalSystem.GetBlockGroupWithName(gNameDrills).GetBlocksOfType<IMyFunctionalBlock>(l2, filterThis);
                if (l2.Count == 0)
                {
                    ERR_TXT += "group " + gNameDrills + " has no Drill blocks\n";
                }
            }
            else
            {
                ERR_TXT += "group" + gNameDrills + " not found\n";
            }

            // user variable declarations
            float myMaxVol_T = 0.0f;
            float myVol_T = 0.0f;
            float myMaxVol = 0.0f;
            float myVol = 0.0f;
            float myCalc = 0.0f;
            float myCalcIce = 0.0f;
            float myCalcStone = 0.0f;
            string myImageOnOff = "";
            string myImageDState = "";

            float myStoneMaxVol = 0.0f;
            float myIceMaxVol = 0.0f;
            float myStoneVol = 0.0f;
            float myIceVol = 0.0f;

            string scrImg;    // Current Immage on LCD


            // display errors
            if (ERR_TXT != "")
            {
                Echo("Script Errors:\n" + ERR_TXT + "(make sure block ownership is set correctly)");
                return;
            }
            else { Echo(""); }
            myMaxVol_T = 0.0f;
            myVol_T = 0.0f;

            if (!string.IsNullOrEmpty(argument))
            {
                if (argument.Trim() == "+")
                {
                    switch (myDrillControl)
                    {
                        case myStateControl.Off:
                            myDrillControl = myStateControl.On;
                            break;
                        default:
                            myDrillControl = myStateControl.Off;
                            break;
                    }
                }
            }


            myMess0 = "Container Status:\n";
            myMess = "";
            String myStorType = "";

            for (int i = 0; i < (int)l0.Count; i++)
            {
                myMaxVol = (float)l0[i].GetInventory(0).MaxVolume;
                myVol = (float)l0[i].GetInventory(0).CurrentVolume;
                myStorType = "o";

                if (myIceList.Contains(l0[i]))
                {
                    myIceMaxVol += myMaxVol;
                    myIceVol += myVol;
                    myStorType = "*";
                }

                if (myStoneList.Contains(l0[i]))
                {
                    myStoneMaxVol += myMaxVol;
                    myStoneVol += myVol;
                    myStorType = "#";
                }

                if (myStoneList.Contains(l0[i]) && myIceList.Contains(l0[i]))
                {
                    myStorType = "x";
                }



                myMaxVol_T += myMaxVol;
                myVol_T += myVol;

                myCalc = (myVol / myMaxVol) * 100f;

                myMess += CalcContFill(i, myCalc, myExtraCargo, myStorType);
                if (i <= myLinesPerScr)
                {
                    myMess0 += myMess;
                    myMess = "";
                }
                else
                {
                    myMess3 += myMess;
                    myMess = "";
                }
            }

            myCalc = (myVol_T / myMaxVol_T) * 100f;
            myCalcStone = (myStoneVol / myStoneMaxVol) * 100f;
            myCalcIce = (myIceVol / myIceMaxVol) * 100f;


            if (myDrillControl == myStateControl.Auto)
            {
                if (myCalc > perAutoOff || myCalcStone > perAutoOff || myCalcIce > perAutoOff)
                {
                    myDrillState = false;
                }
                else
                {
                    if (myCalcStone < perAutoOn || myCalcIce < perAutoOn)
                    {
                        myDrillState = true;
                    }
                }
            }
            else
            {
                if (myDrillControl == myStateControl.Off)
                {
                    myDrillState = false;
                }
                else
                {
                    myDrillState = true;
                    myDrillControl = myStateControl.Auto;
                }
            }

            if (myDrillState)
            {
                myImageDState = "Construction";
            }
            else
            {
                myImageDState = "Cross";
            }

            if (myDrillControl == myStateControl.Auto)
            {
                myImageOnOff = "Construction";
            }
            else
            {
                myImageOnOff = "Cross";
            }

            l2.ForEach(x => ActOnBlock(x, myDrillState));

            myMess4 = "Total " + CalcContFill(-1, myCalc, 1);

            myMess4 += "\nStone " + CalcContFill(-2, myCalcStone, 1);
            myMess4 += "\nIce " + CalcContFill(-3, myCalcIce, 1);

            foreach (string aScreenName in myOutScreen)
            {

                IMyTextSurfaceProvider LCDPanel = GridTerminalSystem.GetBlockWithName(aScreenName) as IMyTextSurfaceProvider;

                IMyTextSurface mesurface0 = LCDPanel.GetSurface(0);
                mesurface0.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface0.FontSize = myFontSizeCargo;
                mesurface0.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                mesurface0.WriteText(myMess0);

                IMyTextSurface mesurface1 = LCDPanel.GetSurface(1);
                mesurface1.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface1.FontSize = 4;
                mesurface1.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                mesurface1.WriteText(Enum.GetName(typeof(myStateControl), myDrillControl) + "\n \n" + myCalc.ToString("N1") + "%");






                IMyTextSurface mesurface2 = LCDPanel.GetSurface(2);
                mesurface2.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface2.FontSize = 2;
                mesurface2.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                scrImg = mesurface2.CurrentlyShownImage;
                if (scrImg == null || myImageOnOff != scrImg)
                {
                    mesurface2.ClearImagesFromSelection();
                    mesurface2.AddImageToSelection(myImageOnOff);
                }


                //mesurface2.WriteText(myMess1);

                IMyTextSurface mesurface3 = LCDPanel.GetSurface(3);
                mesurface3.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface3.FontSize = myFontSizeCargo;
                mesurface3.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                mesurface3.WriteText(myMess3);

                IMyTextSurface mesurface4 = LCDPanel.GetSurface(4);
                mesurface4.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface4.FontSize = 2.1f;
                mesurface4.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                mesurface4.WriteText(myMess4);

                IMyTextSurface mesurface5 = LCDPanel.GetSurface(5);
                mesurface5.ContentType = ContentType.TEXT_AND_IMAGE;
                mesurface5.FontSize = 2;
                mesurface5.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                scrImg = mesurface5.CurrentlyShownImage;
                if (scrImg == null || myImageDState != scrImg)
                {
                    mesurface5.ClearImagesFromSelection();
                    mesurface5.AddImageToSelection(myImageDState);
                }
            }

        }


        // Toggles blocks on/off
        void ActOnBlock(IMyTerminalBlock b, bool setState)
        {
            if (setState == false)
            {
                b.ApplyAction("OnOff_Off");
            }
            else
            {
                b.ApplyAction("OnOff_On");
            }
        }

        string CalcContFill(int no, float myCalc, int extVal = 0, string inTrail = "")
        {
            string myVal = "";
            if (no >= 0)
            { myVal = no.ToString("N0") + " ["; }
            else
            {
                switch (no)
                {
                    case -1:
                        myVal = myCalc.ToString("N0") + "\n[";
                        //myVal = "[";
                        break;
                    case -2:
                        myVal = myCalc.ToString("N0") + "\n[";
                        break;
                    case -3:
                        myVal = myCalc.ToString("N0") + "\n[";
                        break;

                    default:
                        break;
                }
            }
            double Volume = 0;
            Volume = (myCalc / 100) * (myDispWid + extVal);
            for (int i = 0; i < (int)(myDispWid + extVal); i++)
            {
                if (i < Volume) myVal += "I";
                else myVal += " ";
            }
            myVal += "]" + inTrail + "\n";
            return myVal;
        }


        bool filterThis(IMyTerminalBlock block)
        {
            return block.CubeGrid == Me.CubeGrid;
        }
    }
}






