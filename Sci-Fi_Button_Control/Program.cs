﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // This file contains your actual script.
        //
        // You can either keep all your code here, or you can create separate
        // code files to make your program easier to navigate while coding.
        //
        // In order to add a new utility class, right-click on your project, 
        // select 'New' then 'Add Item...'. Now find the 'Space Engineers'
        // category under 'Visual C# Items' on the left hand side, and select
        // 'Utility Class' in the main area. Name it in the box below, and
        // press OK. This utility class will be merged in with your code when
        // deploying your final script.
        //
        // You can also simply create a new utility class manually, you don't
        // have to use the template if you don't want to. Just do so the first
        // time to see what a utility class looks like.
        // 
        // Go to:
        // https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
        //
        // to learn more about ingame scripts.

        const string theButLcd = "Sci-Fi Four-Button Panel (Outside)";



        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set Runtime.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.
            // It's recommended to set Runtime.UpdateFrequency
            // here, which will allow your script to run itself without a
            // timer block.
            //Runtime.UpdateFrequency = UpdateFrequency.None; // Disable self-execution.
            //Runtime.UpdateFrequency = UpdateFrequency.Once; // Run on the next tick, then remove.
            //Runtime.UpdateFrequence = UpdateFrequency.Update1; // Every tick.
            //Runtime.UpdateFrequency = UpdateFrequency.Update10; // Every 10th tick.
            Runtime.UpdateFrequency = UpdateFrequency.Update100; // Every 100th tick.
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from. Be aware that the
            // updateSource is a  bitfield  and might contain more than 
            // one update type.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.

            IMyTextSurfaceProvider lcdButtonPanel = GridTerminalSystem.GetBlockWithName(theButLcd) as IMyTextSurfaceProvider;
            IMyButtonPanel myButPanel = GridTerminalSystem.GetBlockWithName(theButLcd) as IMyButtonPanel;
            if (lcdButtonPanel == null)
            {
                Echo("couldn't find the button panel");
                StopTheScript();
                return;
            }

            //Echo("found the button panel: " + lcdButtonPanel.ToString());
            IMyTextSurface drawingSurface;
            List<ITerminalAction> myActions = new List<ITerminalAction>();

            string butName;
            string myImageOnOff;
            bool myState;
            IMyBlockGroup myBlockGroup;
            IMyTerminalBlock myBlock;
            List<IMyTerminalBlock> myBlocks;
            string ERR_TXT;
            string scrImg;    // Current Immage on LCD


            ERR_TXT = "";
            for (int i = 0; i < 4; i++)
            {
                drawingSurface = lcdButtonPanel.GetSurface(i);
                if (drawingSurface == null)
                {
                    Echo("no drawing surface on the block?");
                    StopTheScript();
                    return;
                }

                butName = myButPanel.GetButtonName(i);
                myBlockGroup = GridTerminalSystem.GetBlockGroupWithName(butName);

                myImageOnOff = "Cross";
                myBlocks = new List<IMyTerminalBlock>();
                if (GridTerminalSystem.GetBlockGroupWithName(butName) != null)
                {
                    GridTerminalSystem.GetBlockGroupWithName(butName).GetBlocksOfType<IMyFunctionalBlock>(myBlocks, filterThis);
                    if (myBlocks.Count == 0)
                    {
                        ERR_TXT += "group " + butName + " has no Functional blocks\n";
                    }

                    myState = GetBlockState(myBlocks[0]);
                    if (myState)
                    {
                        myImageOnOff = "Construction";
                    }
                    else
                    {
                        myImageOnOff = "Cross";
                    }

                }
                else
                {
                    ERR_TXT += "group" + butName + " not found\n";
                }


                butName = butName.Replace(" ", "\n");

                //Echo("I have something to draw on");

                drawingSurface.ContentType = ContentType.TEXT_AND_IMAGE;
                drawingSurface.Alignment = VRage.Game.GUI.TextPanel.TextAlignment.CENTER;
                drawingSurface.FontSize = 2.5f;
                drawingSurface.WriteText(butName);
                scrImg = drawingSurface.CurrentlyShownImage;
                if (scrImg == null || myImageOnOff != scrImg)
                {
                    drawingSurface.ClearImagesFromSelection();
                    drawingSurface.AddImageToSelection(myImageOnOff);
                }

            }

            Echo(ERR_TXT);






        }

        bool GetBlockState(IMyTerminalBlock b)
        {
            return (b as IMyFunctionalBlock).Enabled;
        }

        void ActOnBlock(IMyTerminalBlock b, bool setState)
        {
            if (setState == false)
            {
                b.ApplyAction("OnOff_Off");
            }
            else
            {
                b.ApplyAction("OnOff_On");
            }
        }

        public void StopTheScript()
        {
            Runtime.UpdateFrequency = UpdateFrequency.None;
        }

        bool filterThis(IMyTerminalBlock block)
        {
            return block.CubeGrid == Me.CubeGrid;
        }
    }
}
